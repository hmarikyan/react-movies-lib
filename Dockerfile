FROM node:12.18-alpine as builder
LABEL stage=intermediate

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json
COPY ./public /app/public
COPY ./src /app/src

WORKDIR /app
RUN npm install
RUN npm run build


FROM nginx:stable-alpine
COPY --from=builder /app/build /etc/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
