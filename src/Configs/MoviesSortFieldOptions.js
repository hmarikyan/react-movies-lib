export default [
  {
    value: 'popularity',
    label: 'Popularity',
  },
  {
    value: 'original_title',
    label: 'Title',
  },
  {
    value: 'vote_average',
    label: 'Vote',
  },
  {
    value: 'release_date',
    label: 'Release date'
  }
]
