import __MovieServerConnector from "./__MovieServerConnector";

class MovieService extends __MovieServerConnector{
  getPopularMovies(page) {
    return this.request('/movie/popular', 'GET', {page});
  }

  //data : {query, page}
  searchMovies(data) {
    return this.request('/search/movie', 'GET', data);
  }

  //data : {with_genres, year, page, sort_by}
  filterMovies(data) {
    return this.request('/discover/movie', 'GET', data);
  }

  getMovieById(movieId) {
    return this.request(`/movie/${movieId}`, 'GET');
  }

  getGenres() {
    return this.request('/genre/movie/list', 'GET');
  }

  getFavoriteMovies({accountId, sessionId, page}) {
    return this.request(`/account/${accountId}/favorite/movies`, 'GET', {
      session_id: sessionId,
      page,
    });
  }
}

export default new MovieService();