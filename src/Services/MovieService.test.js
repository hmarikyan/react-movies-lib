import MovieService from "./MovieService";

beforeEach(() => {
  fetch.resetMocks();
});

test('should get movies', async () => {
  fetch.mockResponseOnce(JSON.stringify({
    page: 1,
    results: [
      {
        id: 1,
        genre_ids: [14, 28, 12],
        original_title: "Wonder Woman 1984",
        overview: "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah.",
        popularity: 2173.003,
        poster_path: "/8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg",
        release_date: "2020-12-16",
        title: "Wonder Woman 1984",
        vote_average: 7,
      },
      {
        id: 2,
        genre_ids: [14, 28, 12],
        original_title: "Wonder Woman 1984",
        overview: "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah.",
        popularity: 2173.003,
        poster_path: "/8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg",
        release_date: "2020-12-16",
        title: "Wonder Woman 1984",
        vote_average: 7,
      }
    ],
    total_pages: 500,
    total_results: 10000,
  }));

  const page = 1;

  const res = await MovieService.getPopularMovies(page);
  expect(res).toHaveProperty('page');
  expect(res).toHaveProperty('results');

  expect(res.page).toBe(page);

  expect(Array.isArray(res.results)).toBe(true);
});


test('should throws error', async () => {
  fetch.mockReject(() => Promise.reject("Custom error message"));

  const page = 1

  await expect(MovieService.getPopularMovies(page)).rejects.toEqual('Custom error message');
});