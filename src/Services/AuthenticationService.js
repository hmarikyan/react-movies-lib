import __MovieServerConnector from "./__MovieServerConnector";

const sessionIdKey = 'sessionId';

class AuthenticationService extends __MovieServerConnector {
  getSessionId() {
    return localStorage.getItem(sessionIdKey);
  }

  setSessionId(sessionId) {
    localStorage.setItem(sessionIdKey, sessionId);
  }

  removeSessionId() {
    localStorage.removeItem(sessionIdKey);
  }


  /*user auth requests*/
  createRequestToken() {
    return this.request('/authentication/token/new', 'GET');
  }

  createSessionId(requestToken) {
    return this.request(
      '/authentication/session/new',
      'POST',
      {
        request_token: requestToken,
      }
    );
  }

  login({username, password, requestToken}) {
    return this.request(
      '/authentication/token/validate_with_login',
      'POST',
      {
        username,
        password,
        request_token: requestToken,
      }
    );
  }

  destroySessionId() {
    return this.request(
      '/authentication/session',
      'DELETE',
      {
        session_id: this.getSessionId(),
      }
    );
  }

  getAccount(sessionId = null) {
    let finalSessionId = sessionId;
    if (!finalSessionId) {
      finalSessionId = this.getSessionId();
    }

    return this.request('/account', 'GET', {session_id: finalSessionId});
  }

  /*end user auth requests*/


  toggleFavorite(accountId, mediaId, isFavorite) {
    return this.request(
      `/account/${accountId}/favorite?session_id=${this.getSessionId()}`,
      'POST',
      {
        media_type: 'movie',
        media_id: mediaId,
        favorite: isFavorite,
      }
    );
  }
}

export default new AuthenticationService();