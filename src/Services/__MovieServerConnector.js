import {THE_MOVIE_DB_API_ACCESS_TOKEN, THE_MOVIE_DB_BASE_URL} from "../Configs/AppConfigs";
import {objectToQueryString} from "../Helpers/queryStringHelper";

class __MovieServerConnector {
  async request(path, method, data) {
    let url = THE_MOVIE_DB_BASE_URL + path;

    const requestOptions = {
      method: method,
      cache: 'no-cache',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${THE_MOVIE_DB_API_ACCESS_TOKEN}`
      },
    }
    if (['POST', 'PUT', 'DELETE'].includes(method)) {
      requestOptions.body = JSON.stringify(data);
    } else {
      const paramString = objectToQueryString(data);
      if (paramString) {
        url += '?' + paramString;
      }
    }

    const res = await fetch(url, requestOptions);
    const jsonData = await res.json();

    if (!(res.status >= 200 && res.status <= 299)) {
      //create error msg
      const errorMsg = jsonData.errors?.length ? jsonData.errors : jsonData.status_message ?? 'Internal error';
      throw errorMsg;
    }

    return jsonData;
  }
}

export default __MovieServerConnector;