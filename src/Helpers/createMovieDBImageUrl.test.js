import createMovieDBImageUrl from "./createMovieDBImageUrl";
import {THE_MOVIE_DB_BASE_IMAGE_URL} from "../Configs/AppConfigs";

test('should create a link with default size', () => {

  const imageUrl = createMovieDBImageUrl('example-image.jpg');

  const expected = `${THE_MOVIE_DB_BASE_IMAGE_URL}/w500/example-image.jpg`;

  expect(imageUrl).toBe(expected);
});


test('should create a link with custom size', () => {

  const imageUrl = createMovieDBImageUrl('example-image.jpg', 'original');

  const expected = `${THE_MOVIE_DB_BASE_IMAGE_URL}/original/example-image.jpg`;

  expect(imageUrl).toBe(expected);
});