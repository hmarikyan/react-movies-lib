import {getYearOptions} from "./yearOptions";


test('should get year options', () => {

  const thisYear = (new Date()).getFullYear();

  const result = getYearOptions();

  expect(result).toHaveLength(51);

  expect(result[0].value).toStrictEqual('');

  result.forEach((item) => {
    expect(item).toHaveProperty('value');
    expect(item).toHaveProperty('label');
  });

  expect(result[1].value).toStrictEqual(thisYear);
});
