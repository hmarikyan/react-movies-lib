import {removeEmptyProps, objectToQueryString, queryStringToObject} from './queryStringHelper';

test('should create a string from object', () => {
  const input = {searchTerm: 'avatar', year: 2020};
  const result = objectToQueryString(input);

  const expected = 'searchTerm=avatar&year=2020';

  expect(result).toBe(expected);
});

test('should create an object from string', () => {
  const input = 'searchTerm=avatar&year=2020';
  const result = queryStringToObject(input);

  const expected = {searchTerm: 'avatar', year: '2020'};

  expect(expected).toStrictEqual(result);
});

test('should remove empty props', () => {
  const input = {searchTerm: 'avatar', year: '', genre: null};
  const result = removeEmptyProps(input);

  const expected = {searchTerm: 'avatar'};

  expect(expected).toStrictEqual(result);
});