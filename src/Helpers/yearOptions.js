export const getYearOptions = (startYearsAgo = 50) => {
  let currentYear = (new Date()).getFullYear() - startYearsAgo;

  const options = Array(startYearsAgo)
    .fill(0)
    .map((it, index) => {
      ++currentYear;
      return {
        value: currentYear,
        label: currentYear
      };
    });

  options.reverse();
  options.unshift({
    value: '',
    label: 'Select year'
  });
  return options;
}