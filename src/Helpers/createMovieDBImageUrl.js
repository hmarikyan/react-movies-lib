import {THE_MOVIE_DB_BASE_IMAGE_URL} from "../Configs/AppConfigs";

export default (imageId, size = 'w500') => {
  return `${THE_MOVIE_DB_BASE_IMAGE_URL}/${size}/${imageId}`;
}