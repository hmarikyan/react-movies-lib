export const objectToQueryString = data => (new URLSearchParams(data)).toString();

export const queryStringToObject = qs => {
  let object = {};
  for (let item of new URLSearchParams(qs)) {
    object[item[0]] = item[1];
  }
  return object;
};

export const removeEmptyProps = object => {
  const filteredObject = {};
  for (let key in object) {
    if (object.hasOwnProperty(key) && object[key] !== null && object[key] !== '') {
      filteredObject[key] = object[key];
    }
  }
  return filteredObject;
};