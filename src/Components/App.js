import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import NotFound from "./Common/NotFound/NotFound";
import AuthWrapper from "./AuthWrapper/AuthWrapper";
import LoginContainer from "./LoginContainer/LoginContainer";
import MoviesListContainer from "./MoviesListContainer/MoviesListContainer";
import MovieDetailsContainer from "./MovieDetailsContainer/MovieDetailsContainer";
import FavoriteMoviesListContainer from "./FavoriteMoviesListContainer/FavoriteMoviesListContainer";

import './App.css';


const App = () => {
  return (
    <div className="App">
      <AuthWrapper>
        {(account) => (
          <Router>
            <Switch>
              {!account ? <Route path='/login'>
                <LoginContainer/>
              </Route> : <Route path={'/favorites'}>
                <FavoriteMoviesListContainer/>
              </Route>}
              <Route path="/details/:id">
                <MovieDetailsContainer/>
              </Route>
              <Route path="/" exact>
                <MoviesListContainer/>
              </Route>

              <Route>
                <NotFound/>
              </Route>
            </Switch>
          </Router>
        )}
      </AuthWrapper>
    </div>
  );
}

export default App;
