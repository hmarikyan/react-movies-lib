import {useCallback, useEffect, useReducer} from "react";

import MovieService from "../../Services/MovieService";
import {objectToQueryString} from "../../Helpers/queryStringHelper";
import {defaultSortType} from "../../Configs/SortTypes";

const initialState = {
  movies: [],
  currentPage: 0,
  totalPages: 0,
  isLoading: true,
  error: null,
};

const movieReducer = (state, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        ...state,
        isLoading: action.isLoading
      };
    case 'LOAD_SUCCESS':
      return {
        ...state,
        movies: [...state.movies, ...action.movies],
        currentPage: action.currentPage,
        totalPages: action.totalPages,
        isLoading: false,
      };
    case 'LOAD_FAILURE':
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };
    case 'MARK_MOVIE_FAVORITE':
      return {
        ...state,
        movies: state.movies.map(m => {
          if(m.id === action.movieId) {
            m.isFavorite = action.isFavorite;
          }
          return m;
        }),
      }
    case 'RESET_STATE':
      return {
        ...state,
        ...initialState,
      };
    default:
      throw new Error();
  }
}

const useMoviesList = (queryParams) => {
  const [state, dispatch] = useReducer(movieReducer, initialState);

  const queryString = objectToQueryString(queryParams);

  const loadMoreMovies = useCallback(async (pageToLoad) => {
    try {
      if (!state.isLoading) {
        dispatch({type: 'SET_LOADING', isLoading: true});
      }

      let resData;
      if (queryParams.query) {
        resData = await MovieService.searchMovies({
          query: queryParams.query,
          page: pageToLoad ?? state.currentPage + 1
        });
      } else if (queryParams.year || queryParams.genres || queryParams.sortField) {
        if (queryParams.genres) {
          queryParams.with_genres = queryParams.genres.split('_').join(',');
          delete queryParams.genres;
        }
        if (queryParams.sortField) {
          queryParams.sort_by = queryParams.sortField + '.' + (queryParams.sortType ?? defaultSortType);
          delete queryParams.sortField;
          delete queryParams.sortType;
        }

        resData = await MovieService.filterMovies({
          ...queryParams,
          page: pageToLoad ?? state.currentPage + 1
        });
      } else {
        resData = await MovieService.getPopularMovies(pageToLoad ?? state.currentPage + 1);
      }

      dispatch({
        type: 'LOAD_SUCCESS',
        movies: resData.results,
        currentPage: resData.page,
        totalPages: resData.total_pages,
      });
    } catch (e) {
      dispatch({
        type: 'LOAD_FAILURE',
        error: e,
      });
    }
  }, [queryString, state.isLoading, state.currentPage]);

  const markMovie = useCallback((movieId, isFavorite) => {
    dispatch({
      type: 'MARK_MOVIE_FAVORITE',
      movieId,
      isFavorite
    });
  }, []);

  useEffect(() => {
    dispatch({type: 'RESET_STATE'});
    loadMoreMovies(1);
  }, [state.page, queryString]);

  return {
    movies: state.movies,
    isLoading: state.isLoading,

    currentPage: state.currentPage,
    totalPages: state.totalPages,

    error: state.error,

    loadMoreMovies,
    markMovie,
  }
}

export default useMoviesList;