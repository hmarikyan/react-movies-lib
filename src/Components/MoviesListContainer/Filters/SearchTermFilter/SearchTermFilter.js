import {useMemo, useState} from "react";
import PropTypes from 'prop-types';
import debounce from "../../../../Helpers/debounce";

import TextField from '@material-ui/core/TextField';


const SearchTermFilter = ({queryParams, setQueryParams}) => {

  const [searchTerm, setSearchTerm] = useState(queryParams.query || '');

  const setQueryParamsDebounced = useMemo(() => {
    return debounce(setQueryParams, 1000);
  }, [setQueryParams]);

  const onSearchTermChange = (e) => {
    setSearchTerm(e.target.value);

    setQueryParamsDebounced({
      query: e.target.value
    })
  }

  return <div className={'SearchTermFilter'}>
    <div className={'Filters__Field'}>
      <TextField
        id="searchTerm"
        label="Search"
        value={searchTerm}
        onChange={onSearchTermChange}
      />
    </div>
  </div>
}

SearchTermFilter.propTypes = {
  queryParams: PropTypes.shape({
    query: PropTypes.string,
    year: PropTypes.string,
  }).isRequired,
  setQueryParams: PropTypes.func.isRequired,
}

export default SearchTermFilter;
