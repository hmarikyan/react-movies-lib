import {render, fireEvent, screen, waitFor} from '@testing-library/react';

import Filters from "./Filters";

jest.mock('../../../Services/MovieService', () => {
  return {
    getGenres: async () => {
      return {
        genres: [
          {id: 1, name: 'horror'},
        ]
      }
    }
  }
});


test('should render Simple search', async () => {
  const setQueryParams = jest.fn();
  const queryParams = {};

  const {getByText} = render(<Filters queryParams={queryParams} setQueryParams={setQueryParams}/>);

  await waitFor(() => {
    const simpleSearchLink = getByText(/Simple/ig);

    fireEvent.click(simpleSearchLink);

    expect(screen.getByLabelText('Search')).toBeTruthy();
  });
});

test('should render Advanced search', async () => {
  const setQueryParams = jest.fn();
  const queryParams = {};

  const {getByText} = render(<Filters queryParams={queryParams} setQueryParams={setQueryParams}/>);

  await waitFor(() => {
    const simpleSearchLink = getByText(/Advanced/ig);

    fireEvent.click(simpleSearchLink);

    expect(screen.getByLabelText('Year')).toBeTruthy();
  });
});

test('should render Simple search prefilled', async () => {
  const setQueryParams = jest.fn();
  const queryParams = {query: 'avatar'};

  const {getByText} = render(<Filters queryParams={queryParams} setQueryParams={setQueryParams}/>);

  await waitFor(() => {
    const simpleSearchLink = getByText(/Simple/ig);

    fireEvent.click(simpleSearchLink);

    expect(screen.getByDisplayValue('avatar')).toBeTruthy();
  });
});