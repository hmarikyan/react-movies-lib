import {useState} from "react";
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import SearchTermFilter from "./SearchTermFilter/SearchTermFilter";
import AdvancedFilter from "./AdvancedFilter/AdvancedFilter";
import useGenresList from "../useGenresList";

import './Filters.css';

const Filters = ({queryParams, setQueryParams}) => {
  const {genres: genresList, isLoading} = useGenresList();
  const [filterMode, setSearchTermFilterMode] = useState(0);

  const handleFilterChange = (event, newValue) => {
    setSearchTermFilterMode(newValue);
  };

  if(isLoading) {
    return null;
  }

  return <section className={'Filters'}>
    <div className={'Filters__Inner'}>
      <Tabs value={filterMode} onChange={handleFilterChange}>
        <Tab label="Simple" id={0}/>
        <Tab label="Advanced" id={1}/>
      </Tabs>

      <div className={'Filters__Content'}>
        {filterMode === 0 && <SearchTermFilter queryParams={queryParams} setQueryParams={setQueryParams}/> || null}
        {filterMode === 1 && <AdvancedFilter
          queryParams={queryParams}
          setQueryParams={setQueryParams}
          genresList={genresList}
        /> || null}
      </div>
    </div>

  </section>
}

Filters.propTypes = {
  queryParams: PropTypes.shape({
    query: PropTypes.string,
    year: PropTypes.string,
  }).isRequired,
  setQueryParams: PropTypes.func.isRequired,
}

export default Filters;
