import {useMemo, useState} from "react";
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";
import Input from "@material-ui/core/Input";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

import SortTypes, {defaultSortType} from "../../../../Configs/SortTypes";
import {getYearOptions} from "../../../../Helpers/yearOptions";
import MoviesSortFieldOptions from "../../../../Configs/MoviesSortFieldOptions";

import './AdvancedFilter.css';

const AdvancedFilter = ({queryParams, setQueryParams, genresList}) => {
  const yearOptions = useMemo(() => getYearOptions(100), []);

  const [year, setYear] = useState(queryParams.year || '');
  const [sortField, setSortField] = useState(queryParams.sortField || '');
  const [sortType, setSortType] = useState(queryParams.sortType || defaultSortType);

  const initialGenres = queryParams.genres?.split('_')
    .filter(Boolean)
    .map(i => parseInt(i)) || [];
  const [genre, setGenre] = useState(initialGenres);

  const onYearChange = (e) => {
    setYear(e.target.value);

    setQueryParams({
      ...queryParams,
      year: e.target.value,
      query: null,
    });
  }

  const onGenreChange = (e) => {
    setGenre(e.target.value);

    setQueryParams({
      ...queryParams,
      genres: e.target.value.join('_'),
      query: null,
    });
  }

  const onSortFieldChange = (e) => {
    setSortField(e.target.value);
    setSortType(defaultSortType);
    setQueryParams({
      ...queryParams,
      sortField: e.target.value,
      sortType: defaultSortType,
      query: null,
    });
  }

  const onSortTypeChange = (e) => {
    setSortType(e.target.value);
    setQueryParams({
      ...queryParams,
      sortType: e.target.value,
      query: null,
    });
  }

  return <div className={'AdvancedFilter'}>
    <div className={'AdvancedFilter__Group'}>
      <div className={'Filters__Field'}>
        <FormControl>
          <InputLabel id="year-label">Year</InputLabel>
          <Select
            labelId="year-label"
            id="year"
            value={year}
            onChange={onYearChange}
          >
            {yearOptions.map(option => {
              return <MenuItem key={'_' + option.value} value={option.value}>{option.label}</MenuItem>
            })}
          </Select>
        </FormControl>
      </div>

      <div className={'Filters__Field'}>
        <FormControl>
          <InputLabel id="genres-label">Genres</InputLabel>
          <Select
            labelId="genres-label"
            id="genres"
            multiple
            value={genre}
            onChange={onGenreChange}
            input={<Input/>}
          >
            {genresList.map((option) => {
              return <MenuItem key={option.id} value={option.id}>{option.name}</MenuItem>
            })}
          </Select>
        </FormControl>
      </div>
    </div>

    <div className={'AdvancedFilter__Group'}>
      <div className={'Filters__Field'}>
        <FormControl>
          <InputLabel id="sortfield-label">Sort field</InputLabel>
          <Select
            labelId="sortfield-label"
            id="sortField"
            value={sortField}
            onChange={onSortFieldChange}
            input={<Input/>}
          >
            {MoviesSortFieldOptions.map((option) => {
              return <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
            })}
          </Select>
        </FormControl>
      </div>

      <div className={'Filters__Field'}>
        <FormControl>
          <InputLabel id="sorttype-label">Sort type</InputLabel>
          <Select
            disabled={!sortField}
            labelId="sorttype-label"
            id="sortType"
            value={sortType}
            onChange={onSortTypeChange}
            input={<Input/>}
          >
            {SortTypes.map((option) => {
              return <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
            })}
          </Select>
        </FormControl>
      </div>
    </div>
  </div>
}

AdvancedFilter.propTypes = {
  queryParams: PropTypes.shape({
    query: PropTypes.string,
    year: PropTypes.string,
  }).isRequired,
  setQueryParams: PropTypes.func.isRequired,
  genresList: PropTypes.array,
}

export default AdvancedFilter;
