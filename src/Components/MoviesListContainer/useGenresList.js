import {useCallback, useEffect, useReducer} from "react";

import MovieService from "../../Services/MovieService";

const initialState = {
  genres: [],
  isLoading: true,
  error: null,
};

const genreReducer = (state, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        ...state,
        isLoading: action.isLoading
      };
    case 'LOAD_SUCCESS':
      return {
        ...state,
        genres: action.genres,
        isLoading: false,
      };
    case 'LOAD_FAILURE':
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };
    case 'RESET_STATE':
      return {
        ...state,
        ...initialState,
      };
    default:
      throw new Error();
  }
}

const useGenresList = () => {
  const [state, dispatch] = useReducer(genreReducer, initialState);

  const loadGenres = useCallback(async () => {
    try {
      if (!state.isLoading) {
        dispatch({type: 'SET_LOADING', isLoading: true});
      }

      const resData = await MovieService.getGenres();

      dispatch({
        type: 'LOAD_SUCCESS',
        genres: resData.genres,
      });
    } catch (e) {
      dispatch({
        type: 'LOAD_FAILURE',
        error: e,
      });
    }
  }, [state.isLoading]);

  useEffect(() => {
    loadGenres();
  }, []);

  return {
    genres: state.genres,
    isLoading: state.isLoading,
    error: state.error,
  }
}

export default useGenresList;