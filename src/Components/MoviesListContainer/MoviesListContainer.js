import {useContext} from "react";

import AuthenticationService from "../../Services/AuthenticationService";
import useMoviesList from "./useMoviesList";
import useInfiniteScroll from "../../Hooks/useInfiniteScroll";
import useQueryParamsManipulation from "../../Hooks/useQueryParamsManipulation";
import useMetaTitle from "../../Hooks/useMetaTitle";
import AuthContext from "../../Contexts/AuthContext";

import CenterLoader from "../Common/Loader/CenterLoader";
import NotFound from "../Common/NotFound/NotFound";
import Error from "../Common/Error/Error";
import PageContainer from "../Common/PageContainer/PageContainer";

import MovieCard from "../MovieCard/MovieCard";
import PageHeader from "../PageHeader/PageHeader";
import Filters from "./Filters/Filters";

import './MoviesListContainer.css';


const MoviesListContainer = () => {
  const {account} = useContext(AuthContext);

  const {queryParams, setQueryParams} = useQueryParamsManipulation();
  const {movies, currentPage, totalPages, isLoading, error, loadMoreMovies, markMovie} = useMoviesList(queryParams);

  useInfiniteScroll(loadMoreMovies, !isLoading && movies.length && currentPage < totalPages);

  useMetaTitle('Movies lib');

  const onFavoriteClick = async (movieId, isFavorite) => {
    await AuthenticationService.toggleFavorite(account.id, movieId, isFavorite);
    markMovie(movieId, isFavorite);
  }

  return <PageContainer>
    <PageHeader
      pageTitle={<div>Movies</div>}
    />
    <div className={'MoviesListContainer'}>
      <Filters queryParams={queryParams} setQueryParams={setQueryParams}/>

      {!error ? (movies.length ? <section className={'MoviesListContainer__MoviesContent'}>
        {movies.map(movie => <MovieCard
          {...movie}
          key={movie.id}
          isFavorite={movie.isFavorite ?? false}
          allowMarkFavorite={!!account}
          onFavoriteClick={onFavoriteClick}
        />)}
      </section> : (isLoading ? <CenterLoader/> : <NotFound/>)) : <Error message={error}/>}

      {isLoading && movies.length ? <CenterLoader/> : null}
    </div>
  </PageContainer>
}

export default MoviesListContainer;