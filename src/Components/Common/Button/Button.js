import PropTypes from 'prop-types';

import './Button.css';

const Button = ({
                  children,
                  disabled,
                  className,
                  primary,
                  type,
                  fluid,
                  size,

                  onClick,
                }) => {
  const classNames = ['Button'];
  if (className) {
    classNames.push(className);
  }
  classNames.push(primary ? 'primary' : 'secondary')
  if (fluid) {
    classNames.push('fluid');
  }
  classNames.push(size);

  return <button
    className={classNames.join(' ')}
    disabled={disabled}
    type={type}
    onClick={onClick}
  >
    {children}
  </button>
}

Button.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  primary: PropTypes.bool,
  type: PropTypes.oneOf(['button', 'submit']),
  fluid: PropTypes.bool,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  onClick: PropTypes.func,
}

Button.defaultProps = {
  disabled: false,
  className: '',
  primary: true,
  type: 'button',
  fluid: false,
  size: 'medium',
  onClick: () => null,
}

export default Button;