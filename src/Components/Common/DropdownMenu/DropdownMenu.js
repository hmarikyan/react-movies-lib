import {useCallback, useEffect, useRef, useState} from "react";
import PropTypes from 'prop-types';

import './DropdownMenu.css';


const DropdownMenu = ({label, children, defaultIsOpen, className}) => {
  const [isOpen, setOpen] = useState(defaultIsOpen);

  const dropdownMenuRef = useRef(null);

  const classNames = ['DropdownMenu'];
  if (className) {
    classNames.push(className);
  }
  if (isOpen) {
    classNames.push('open');
  }

  const toggleOpen = useCallback((newOpenState = false) => {
    setOpen(newOpenState);
  }, [setOpen]);

  const handleClickOutside = useCallback((event) => {
    if (dropdownMenuRef && !dropdownMenuRef.current.contains(event.target)) {
      toggleOpen(false);
    }
  }, [dropdownMenuRef, toggleOpen]);

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    }
  }, [handleClickOutside]);

  return <div ref={dropdownMenuRef} className={classNames.join(' ')}>
    <div
      className={'DropdownMenu__Label'}
      onClick={() => toggleOpen(!isOpen)}
    >
      {label} <span>{isOpen ? '↑' : '↓'}</span>
    </div>

    <div className={'DropdownMenu__ListWrapper'}>
      <div className={'DropdownMenu__List'}>
        {children}
      </div>
    </div>
  </div>
}

DropdownMenu.propTypes = {
  items: PropTypes.array,
  label: PropTypes.node,
  className: PropTypes.string,
  defaultIsOpen: PropTypes.bool,
};

DropdownMenu.defaultProps = {
  defaultIsOpen: false,
}

export default DropdownMenu;