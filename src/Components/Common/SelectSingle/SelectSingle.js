import PropTypes from 'prop-types';
import './SelectSingle.css';

const SelectSingle = ({
                        value,
                        onChange,
                        options,
                        onBlur,

                        className,
                        id,
                      }) => {

  const classNames = ['SelectSingle'];
  if (className) {
    classNames.push(className)
  }

  return <select
    value={value}
    onChange={onChange}
    onBlur={onBlur}
    className={classNames.join('')}
    id={id}
  >
    {options.map(option => <option
        key={'_' + option.value}
        value={option.value}
      >
        {option.label}
      </option>
    )}
  </select>
}

SelectSingle.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,

  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      label: PropTypes.node
    })
  ),

  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ])
}

SelectSingle.defaultProps = {
  value: '',
  onBlur: () => null,
  className: '',
  id: '',
}

export default SelectSingle;