import './NotFound.css';

const NotFound = () => {
  return <div className={'NotFound'}>
    <h2>No result</h2>
  </div>
}

export default NotFound;