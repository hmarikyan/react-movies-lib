import './Error.css';
import PropTypes from "prop-types";

const Error = ({message}) => {
  return <div className={'Error'}>
    <h2>{message ?? 'Something went wrong'}</h2>
  </div>
}

Error.propTypes = {
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ])
}

export default Error;