import PropTypes from 'prop-types';

import './Sticker.css';

const Sticker = ({children}) => {
  return <div className={'Sticker'}>
    {children}
  </div>
}

Sticker.propTypes = {
  children: PropTypes.node,
}

export default Sticker;