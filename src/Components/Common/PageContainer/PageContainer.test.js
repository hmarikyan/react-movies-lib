import { render, screen } from '@testing-library/react';
import PageContainer from './PageContainer';

test('should render PageContainer', () => {
  render(<PageContainer>App</PageContainer>);

  const result = screen.getByText(/App/i);
  expect(result).toBeInTheDocument();
});
