import PropTypes from 'prop-types';
import './PageContainer.css';

const PageContainer = ({children}) => {
  return <div className={'PageContainer'}>
    {children}
  </div>;
}

PageContainer.propTypes = {
  children: PropTypes.any,
}

export default PageContainer;