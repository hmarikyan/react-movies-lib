import {Link as RouterLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import './Link.css';

const Link = (props) => {
  const classNames = ['Link'];
  if (props.className) {
    classNames.push(props.className);
  }

  return <RouterLink {...props} className={classNames.join(' ')}>{props.children}</RouterLink>
}

Link.propTypes = {
  children: PropTypes.node,
  to: PropTypes.any,
  className: PropTypes.string,
}

export default Link;
