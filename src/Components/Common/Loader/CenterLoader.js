import Loader from "./Loader";
import './CenterLoader.css';

const CenterLoader = () => {
  return <div className={'CenterLoader'}>
    <Loader />
  </div>
}

export default CenterLoader