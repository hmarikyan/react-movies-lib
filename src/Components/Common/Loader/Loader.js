import './Loader.css';

const Loader = () => {
  return <div className={'Loader'}>
    <div className="Loader__Container">
      <div className="Loader__Container__dot Loader__Container__dot-1"></div>
      <div className="Loader__Container__dot Loader__Container__dot-2"></div>
      <div className="Loader__Container__dot Loader__Container__dot-3"></div>
    </div>
  </div>
}

export default Loader