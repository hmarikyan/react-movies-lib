import {useParams} from 'react-router-dom';

import useMetaTitle from "../../Hooks/useMetaTitle";
import useMovieDetails from "./useMovieDetails";
import Error from "../Common/Error/Error";

import PageContainer from "../Common/PageContainer/PageContainer";
import Link from "../Common/Link/Link";
import CenterLoader from "../Common/Loader/CenterLoader";
import MovieDetails from "./MovieDetails/MovieDetails";
import PageHeader from "../PageHeader/PageHeader";


const MovieDetailsContainer = () => {
  const {id} = useParams();

  const {movie, isLoading, error} = useMovieDetails(id);

  useMetaTitle(movie?.title ?? '', true);

  return <PageContainer>
    <PageHeader
      pageTitle={<div><Link to={'/'}>Movies</Link> -> {movie?.title}</div>}
    />
    <div className={'MovieDetailsContainer'}>
      {isLoading ? <CenterLoader/> :
        error ? <Error message={error}/> :
          <MovieDetails movie={movie}/>}
    </div>
  </PageContainer>
}

export default MovieDetailsContainer;