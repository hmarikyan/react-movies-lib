import PropTypes from "prop-types";

import './GenresList.css';

const GenresList = ({genres}) => {
  return <div className={'GenresList'}>
    {genres.map(genre => <div className={'GenresList_Item'} key={genre.id}>
      {genre.name}
    </div>)}
  </div>
}

GenresList.propTypes = {
  genres: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  )
}


export default GenresList;