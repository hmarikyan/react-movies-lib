import {useCallback, useEffect, useReducer} from "react";

import MovieService from "../../Services/MovieService";

const initialState = {
  movie: null,
  isLoading: true,
  error: null,
};

const movieDetailsReducer = (state, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        ...state,
        isLoading: action.isLoading
      };
    case 'LOAD_SUCCESS':
      return {
        ...state,
        movie: action.movie,
        isLoading: false,
      };
    case 'LOAD_FAILURE':
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };
    case 'RESET_STATE':
      return {
        ...state,
        ...initialState,
      };
    default:
      throw new Error();
  }
}

const useMovieDetails = (movieId) => {
  const [state, dispatch] = useReducer(movieDetailsReducer, initialState);

  const loadMovie = useCallback(async (movieId) => {
    try {
      if (!state.isLoading) {
        dispatch({type: 'SET_LOADING', isLoading: true});
      }

      if(Number.isNaN(parseInt(movieId))) {
        throw 'Movie ID is invalid.';
      }

      const resData = await MovieService.getMovieById(movieId);

      dispatch({
        type: 'LOAD_SUCCESS',
        movie: resData,
      });
    } catch (e) {
      dispatch({
        type: 'LOAD_FAILURE',
        error: e,
      });
    }
  }, [state.isLoading]);

  useEffect(() => {
    loadMovie(movieId);
  }, []);

  return {
    movie: state.movie,
    isLoading: state.isLoading,
    error: state.error,
  }
}

export default useMovieDetails;