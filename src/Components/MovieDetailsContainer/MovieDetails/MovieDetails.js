import PropTypes from "prop-types";

import createMovieDBImageUrl from "../../../Helpers/createMovieDBImageUrl";
import DefaultMovieIcon from "../../../Assets/Images/default-movie.png";
import GenresList from "../GenresList/GenresList";
import Sticker from "../../Common/Sticker/Sticker";

import './MovieDetails.css';


const MovieDetails = ({movie}) => {

  const posterUrl = movie.poster_path ? createMovieDBImageUrl(movie.poster_path) : DefaultMovieIcon;

  return <div className={'MovieDetails'}>
    <div className={'MovieDetails__PosterWrapper'}>
      <div className={'MovieDetails_Poster'}>
        <img src={posterUrl} alt={movie.title} />
        <Sticker>{movie.vote_average}</Sticker>
      </div>
    </div>

    <div className={'MovieDetails__ContentWrapper'}>
      <div className={'MovieDetails_Title'}>
        <h1>{movie.title}</h1>
        <p>{movie.tagline}</p>
      </div>

      <div className={'MovieDetails_Genres'}>
        <GenresList genres={movie.genres} />
      </div>

      <div className={'MovieDetails_Overview'}>
        <p>{movie.overview}</p>
      </div>

      <div className={'MovieDetails_ReleaseDate'}>
        <div>
          Release date:
        </div>
        <div>
          {movie.release_date}
        </div>
      </div>
    </div>
  </div>
}

MovieDetails.propTypes = {
  movie: PropTypes.shape({
    title: PropTypes.string,
    overview: PropTypes.string,
  })
}

export default MovieDetails;