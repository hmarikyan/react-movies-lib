import PropTypes from 'prop-types';
import './PageTitle.css';

const PageTitle = ({children}) => {
  return <section className={'PageTitle'}>
    <h1>{children}</h1>
  </section>
}

PageTitle.propTypes = {
  children: PropTypes.node,
}

export default PageTitle;