import {useContext} from "react";
import {useHistory} from "react-router-dom";

import AuthContext from "../../Contexts/AuthContext";
import PageTitle from "./PageTitle/PageTitle";
import DropdownMenu from "../Common/DropdownMenu/DropdownMenu";
import Link from "../Common/Link/Link";

import './PageHeader.css';


const PageHeader = ({pageTitle}) => {
  const {account, doLogout, isLoading} = useContext(AuthContext);
  const history = useHistory();

  const logout = async () => {
    await doLogout();
    history.push('/');
  }

  return <div className={'PageHeader'}>
    <PageTitle>{pageTitle}</PageTitle>

    {!isLoading && <div className={'PageHeader__AuthSection'}>
      {account ? <div className={'PageHeader__AuthSection__LoggedIn'}>
        <DropdownMenu label={account.username}>
          <ul className={'PageHeader__UserDropdownItems'}>
            <li onClick={() => history.push('/favorites')}>
              Favorites
            </li>
            <li onClick={logout}>
              Logout
            </li>
          </ul>
        </DropdownMenu>
      </div> : <div className={'PageHeader__AuthSection__Anonymous'}>
        <Link to={'/login'}>Login</Link>
      </div>}
    </div>}
  </div>
}

export default PageHeader;