import PropTypes from 'prop-types';

import createMovieDBImageUrl from "../../Helpers/createMovieDBImageUrl";
import Button from "../Common/Button/Button";
import Sticker from "../Common/Sticker/Sticker";
import Link from "../Common/Link/Link";

import DefaultMovieIcon from '../../Assets/Images/default-movie.png';
import './MovieCard.css';

const MovieCard = ({
                     id,
                     title,
                     release_date,
                     vote_average,
                     poster_path,

                     allowMarkFavorite,
                     isFavorite,
                     onFavoriteClick,
                   }) => {
  const imageUrl = poster_path ? createMovieDBImageUrl(poster_path) : DefaultMovieIcon;

  return <div className="MovieCard">
    <div className="MovieCard__Inner">
      <div className={'MovieCard__Poster'}>
        <img
          src={imageUrl}
          alt={title}
          className={'MovieCard__Poster'}
        />
        <Sticker>{vote_average}</Sticker>
      </div>

      <div className="MovieCard__Content">
        <div className={'MovieCard__Content__Title'}>
          <h4>
            <b>
              <Link to={`/details/${id}`}>{title}</Link>
            </b>
          </h4>
          <p className={'MovieCard__Content__ReleaseDate'}>{release_date}</p>
        </div>

        <div className={'MovieCard__Content__Footer'}>


          {allowMarkFavorite ? <div>
            <Button size={'small'} onClick={() => onFavoriteClick(id, !isFavorite)}>
              {isFavorite ? 'Unmark ' : 'Mark as favorite'}
            </Button>
          </div> : null}
        </div>
      </div>
    </div>
  </div>
}

MovieCard.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  title: PropTypes.string,
  release_date: PropTypes.string,
  vote_average: PropTypes.number,
  poster_path: PropTypes.string,

  isFavorite: PropTypes.bool,
  allowMarkFavorite: PropTypes.bool,
  onFavoriteClick: PropTypes.func,

};


export default MovieCard;