import {useCallback, useEffect, useReducer} from "react";

import MovieService from "../../Services/MovieService";

const initialState = {
  movies: [],
  currentPage: 0,
  totalPages: 0,
  isLoading: true,
  error: null,
};

const movieReducer = (state, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        ...state,
        isLoading: action.isLoading
      };
    case 'LOAD_SUCCESS':
      return {
        ...state,
        movies: [...state.movies, ...action.movies],
        currentPage: action.currentPage,
        totalPages: action.totalPages,
        isLoading: false,
      };
    case 'REMOVE_MOVIE':
      return {
        ...state,
        movies: state.movies.filter(m => m.id !== action.movieId),
      };
    case 'LOAD_FAILURE':
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };
    case 'RESET_STATE':
      return {
        ...state,
        ...initialState,
      };
    default:
      throw new Error();
  }
}

const useFavoriteMoviesList = (accountId, sessionId) => {
  const [state, dispatch] = useReducer(movieReducer, initialState);

  const loadMoreMovies = useCallback(async (pageToLoad) => {
    try {
      if (!state.isLoading) {
        dispatch({type: 'SET_LOADING', isLoading: true});
      }

      const resData = await MovieService.getFavoriteMovies({
        accountId,
        sessionId,
        page: pageToLoad ?? state.currentPage + 1
      });

      dispatch({
        type: 'LOAD_SUCCESS',
        movies: resData.results,
        currentPage: resData.page,
        totalPages: resData.total_pages,
      });
    } catch (e) {
      dispatch({
        type: 'LOAD_FAILURE',
        error: e,
      });
    }
  }, [state.isLoading, state.currentPage, accountId, sessionId]);

  const removeMovie = useCallback((movieId) => {
    dispatch({
      type: 'REMOVE_MOVIE',
      movieId,
    });
  }, []);

  useEffect(() => {
    dispatch({type: 'RESET_STATE'});
    loadMoreMovies(1);
  }, []);

  return {
    movies: state.movies,
    isLoading: state.isLoading,

    currentPage: state.currentPage,
    totalPages: state.totalPages,

    error: state.error,

    loadMoreMovies,
    removeMovie,
  }
}

export default useFavoriteMoviesList;