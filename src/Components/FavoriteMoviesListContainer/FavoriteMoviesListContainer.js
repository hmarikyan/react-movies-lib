import {useContext} from "react";

import useFavoriteMoviesList from "./useFavoriteMoviesList";
import useInfiniteScroll from "../../Hooks/useInfiniteScroll";
import useMetaTitle from "../../Hooks/useMetaTitle";
import Link from "../Common/Link/Link";
import CenterLoader from "../Common/Loader/CenterLoader";
import NotFound from "../Common/NotFound/NotFound";
import Error from "../Common/Error/Error";
import PageContainer from "../Common/PageContainer/PageContainer";
import AuthContext from "../../Contexts/AuthContext";
import AuthenticationService from "../../Services/AuthenticationService";
import MovieCard from "../MovieCard/MovieCard";
import PageHeader from "../PageHeader/PageHeader";

import './FavoriteMoviesListContainer.css';


const FavoriteMoviesListContainer = () => {
  const {account} = useContext(AuthContext);
  const sessionId = AuthenticationService.getSessionId();

  const {movies, currentPage, totalPages, isLoading, error, loadMoreMovies, removeMovie} = useFavoriteMoviesList(account.id, sessionId);

  useInfiniteScroll(loadMoreMovies, !isLoading && movies.length && currentPage < totalPages);

  useMetaTitle('Movies lib - favorites');

  const onFavoriteClick = (movieId, isFavorite) => {
    AuthenticationService.toggleFavorite(account.id, movieId, isFavorite);
    removeMovie(movieId);
  }

  return <PageContainer>
    <PageHeader
      pageTitle={<div><Link to={'/'}>Movies</Link> -> Favorite</div>}
    />
    <div className={'FavoriteMoviesListContainer'}>
      {!error ? (movies.length ? <section className={'FavoriteMoviesListContainer__MoviesContent'}>
        {movies.map(movie => <MovieCard
          {...movie}
          key={movie.id}
          isFavorite={true}
          allowMarkFavorite={!!account}
          onFavoriteClick={onFavoriteClick}
        />)}
      </section> : (isLoading ? <CenterLoader/> : <NotFound/>)) : <Error message={error}/>}

      {isLoading && movies.length ? <CenterLoader/> : null}
    </div>
  </PageContainer>
}

export default FavoriteMoviesListContainer;