import {useEffect, useState} from "react";
import AuthenticationService from "../../Services/AuthenticationService";

const useAuth = () => {
  const [account, setAccount] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const getAccount = async () => {
    try {
      !isLoading && setLoading(true);
      const account = await AuthenticationService.getAccount();
      if (account.id) {
        setAccount(account);
      } else {
        //remove session id because its not valid
        AuthenticationService.removeSessionId();
      }
    } finally {
      setLoading(false);
    }
  };

  const doLogin = async ({username, password}) => {
    !isLoading && setLoading(true);

    try {
      const {success: requestTokenSuccess, request_token: requestToken} = await AuthenticationService.createRequestToken();
      if (requestTokenSuccess) {
        const {success: loginSuccess, request_token: accountRequestToken} = await AuthenticationService.login({
          requestToken,
          username,
          password
        });
        if (loginSuccess) {
          const {success: accountSuccess, session_id: sessionId} = await AuthenticationService.createSessionId(accountRequestToken);
          if (accountSuccess) {
            AuthenticationService.setSessionId(sessionId);
            await getAccount();
          } else {
            throw 'Get Account - failed';
          }

        } else {
          throw 'User login - failed';
        }
      } else {
        throw 'Create request token - failed';
      }
    } finally {
      setLoading(false);
    }
  };

  const doLogout = () => {
    try {
      !isLoading && setLoading(true);

      if (AuthenticationService.getSessionId()) {
        AuthenticationService.destroySessionId();
        AuthenticationService.removeSessionId();
        setAccount(null);
      }
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (AuthenticationService.getSessionId()) {
      getAccount();
    }
  }, []);

  return {account, doLogin, doLogout, isLoading};
}

export default useAuth;