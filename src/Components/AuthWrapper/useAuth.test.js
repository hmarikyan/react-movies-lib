import {act, renderHook} from "@testing-library/react-hooks";

import useAuth from './useAuth';
import AuthenticationService from "../../Services/AuthenticationService";


jest.mock('../../Services/AuthenticationService');

beforeEach(() => {
  jest.clearAllMocks();
})

test('should return account and isLoading flag correctly', async () => {
  AuthenticationService.getSessionId.mockReturnValue('session_1');
  AuthenticationService.getAccount.mockResolvedValue({
    id: 'id_1',
    username: 'user_1'
  });

  const {result, waitForNextUpdate} = renderHook(() => useAuth());

  expect(result.current.isLoading).toBe(true);
  expect(result.current.account).toBe(null);

  await waitForNextUpdate();

  expect(result.current.account).toHaveProperty('id');
  expect(result.current.isLoading).toBe(false);
});


test('should login correctly', async () => {
  //mock auth functions
  AuthenticationService.getSessionId.mockReturnValue(null);
  AuthenticationService.setSessionId.mockReturnValue(null);
  AuthenticationService.createRequestToken.mockResolvedValue({
    success: true,
    request_token: 'token__1'
  });
  AuthenticationService.login.mockResolvedValue({
    success: true,
    request_token: 'request_token_1'
  });
  AuthenticationService.createSessionId.mockResolvedValue({
    success: true,
    session_id: 'session_id'
  });
  AuthenticationService.getAccount.mockResolvedValue({
    id: 'id_1',
    username: 'user_1'
  });

  const {result, waitForNextUpdate, rerender} = renderHook(() => useAuth());

  //initially the use should be empty because there is no session_id
  expect(result.current.isLoading).toBe(false);
  expect(result.current.account).toBe(null);

  //do login
  act(() => {
    result.current.doLogin({username: 'h', password: '1'})
  })

  //wait for async updates
  await waitForNextUpdate();

  //now account should be fulfilled
  expect(result.current.account).toHaveProperty('id');
  expect(result.current.isLoading).toBe(false);

  //rerender should keep account data
  rerender()

  expect(result.current.account).toHaveProperty('id');
})
