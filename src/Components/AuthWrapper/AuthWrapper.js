import useAuth from "./useAuth";
import AuthContext from "../../Contexts/AuthContext";

const AuthWrapper = ({children}) => {
  //isLoading, account, doLogin, doLogout
  const loginProps = useAuth();

  //pass data to context which will allow us to get data from child components
  return <AuthContext.Provider value={loginProps}>
    {children(loginProps.account, loginProps.isLoading)}
  </AuthContext.Provider>
}

export default AuthWrapper;