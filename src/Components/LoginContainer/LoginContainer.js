import {useContext, useState} from "react";
import {useHistory} from "react-router-dom";

import {DEFAULT_USER_CREDENTIALS} from "../../Configs/AppConfigs";
import AuthContext from "../../Contexts/AuthContext";
import PageContainer from "../Common/PageContainer/PageContainer";
import CenterLoader from "../Common/Loader/CenterLoader";
import TextInput from "../Common/TextInput/TextInput";
import Button from "../Common/Button/Button";

import './LoginContainer.css';


const LoginContainer = () => {
  const {doLogin, isLoading: isAuthLoading} = useContext(AuthContext);
  const [errorMsg, setErrorMsg] = useState(null);

  const [username, setUsername] = useState(DEFAULT_USER_CREDENTIALS.USERNAME);
  const [password, setPassword] = useState(DEFAULT_USER_CREDENTIALS.PASSWORD);

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();
    try {
      errorMsg && setErrorMsg(null);

      await doLogin({username, password});
      history.push('/');

    } catch (e) {
      setErrorMsg(e);
    }
  }

  return <PageContainer>
    <div className={'LoginContainer'}>
      <div className={'LoginContainer__Inner'}>
        {isAuthLoading ? <CenterLoader/> : null}

        <form onSubmit={login}>
          <div>
            <h1>Login form</h1>
          </div>

          <div className="container">
            <div className={'LoginContainer__Form__Control'}>
              {errorMsg && <span className={'LoginContainer__Form__Alert'}>{errorMsg}</span> || null}
            </div>

            <div className={'LoginContainer__Form__Control'}>
              <label htmlFor="uname"><b>Username</b></label>
              <TextInput
                id={'uname'}
                value={username}
                onChange={({target}) => setUsername(target.value)}
              />
            </div>

            <div className={'LoginContainer__Form__Control'}>
              <label htmlFor="psw"><b>Password</b></label>
              <TextInput
                type={'password'}
                id={'psw'}
                value={password}
                onChange={({target}) => setPassword(target.value)}
              />
            </div>

            <div className={'LoginContainer__Form__Control'}>
              <Button
                fluid
                type="submit"
                disabled={isAuthLoading}
              >
                Login
              </Button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </PageContainer>
}

export default LoginContainer;