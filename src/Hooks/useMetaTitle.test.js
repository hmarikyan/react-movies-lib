import {renderHook} from '@testing-library/react-hooks';

import useMetaTitle from "./useMetaTitle";


test('should set title', () => {
  const { result } = renderHook(() => useMetaTitle('Custom title'))
  expect(window.document.title).toBe('Custom title')
})

test('should append title', () => {
  const { result, rerender } = renderHook(({title, shouldAppend}) => useMetaTitle(title, shouldAppend), {
    initialProps: { title: 'Custom Title', shouldAppend: false }
  });

  rerender({title: 'Appended', shouldAppend: true});

  expect(window.document.title).toBe('Custom Title - Appended')
})