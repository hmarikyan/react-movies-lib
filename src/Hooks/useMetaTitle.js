import {useEffect} from "react";

const useMetaTitle = (title, shouldAppend = false) => {

  useEffect(() => {
    const oldTitle = document.title;

    document.title = shouldAppend ? oldTitle + ' - '+ title : title;
  }, [title, shouldAppend]);
}


export default useMetaTitle;