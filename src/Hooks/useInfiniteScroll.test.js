import {renderHook, act} from "@testing-library/react-hooks";
import useInfiniteScroll from "./useInfiniteScroll";

beforeEach(() => {
  jest
    .spyOn(document.documentElement, 'scrollHeight', 'get')
    .mockImplementation(() => 1000);
  jest
    .spyOn(document.documentElement, 'clientHeight', 'get')
    .mockImplementation(() => 500);
  jest
    .spyOn(document.documentElement, 'scrollTop', 'get')
    .mockImplementation(() => 0);
});

test('should invoke callback', () => {
  const requestLoadFn = jest.fn();

  const { result, rerender } = renderHook(({requestLoadFn, hasNextPage}) => useInfiniteScroll(requestLoadFn, hasNextPage), {
    initialProps: {
      requestLoadFn: requestLoadFn,
      hasNextPage: true,
    }
  });

  jest
    .spyOn(document.documentElement, 'scrollTop', 'get')
    .mockImplementation(() => 500);
  window.dispatchEvent(new Event('scroll'));

  expect(requestLoadFn).toBeCalled();
})


test('should not invoke callback', () => {
  const requestLoadFn = jest.fn();

  const { result, rerender } = renderHook(({requestLoadFn, hasNextPage}) => useInfiniteScroll(requestLoadFn, hasNextPage), {
    initialProps: {
      requestLoadFn: requestLoadFn,
      hasNextPage: false,
    }
  });

  jest
    .spyOn(document.documentElement, 'scrollTop', 'get')
    .mockImplementation(() => 500);
  window.dispatchEvent(new Event('scroll'));

  expect(requestLoadFn).not.toBeCalled();
});


test('should invoke callback once', () => {
  const requestLoadFn = jest.fn();

  const { result, rerender } = renderHook(({requestLoadFn, hasNextPage}) => useInfiniteScroll(requestLoadFn, hasNextPage), {
    initialProps: {
      requestLoadFn: requestLoadFn,
      hasNextPage: true,
    }
  });

  jest
    .spyOn(document.documentElement, 'scrollTop', 'get')
    .mockImplementation(() => 500);
  window.dispatchEvent(new Event('scroll'));

  expect(requestLoadFn).toBeCalled();

  //reset scroll
  jest
    .spyOn(document.documentElement, 'scrollTop', 'get')
    .mockImplementation(() => 0);

  const requestLoadFn2 = jest.fn();
  rerender({requestLoadFn: requestLoadFn2, hasNextPage: false});

  jest
    .spyOn(document.documentElement, 'scrollTop', 'get')
    .mockImplementation(() => 500);
  window.dispatchEvent(new Event('scroll'));

  expect(requestLoadFn2).not.toBeCalled();
})

