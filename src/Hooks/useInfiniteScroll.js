import {useEffect} from "react";

const useInfiniteScroll = (requestLoadFn, hasNextPage) => {

  useEffect(() => {
    const onScroll = () => {
      const {scrollHeight, scrollTop, clientHeight} = window.document.documentElement;
      //reaches to bottom of the page
      if (scrollTop + clientHeight >= scrollHeight ) {
        requestLoadFn();
        window.removeEventListener('scroll', onScroll);
      }
    }

    //attach listener when the next page is available
    if (hasNextPage) {
      window.addEventListener('scroll', onScroll);
    }

    return () => {
      window.removeEventListener('scroll', onScroll);
    }
  }, [requestLoadFn, hasNextPage]);

  return null;
}

export default useInfiniteScroll;