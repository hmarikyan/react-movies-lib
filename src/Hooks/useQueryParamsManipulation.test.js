import {renderHook, act} from "@testing-library/react-hooks";
import useQueryParamsManipulation from "./useQueryParamsManipulation";

jest.mock('react-router-dom', () => {
  let queryString = '?searchTerm=avatar';

  return {
    useLocation: () => ({
      pathname: '/',
      search: queryString,
      hash: '',
      state: null,
      key: '5nvxpbdafa',
    }),
    useHistory: () => ({
      push: (path) => {
        queryString = '?'+path?.split('?')[1];
      },
    }),
  };
});

test('should get query params', () => {
  const {result, rerender} = renderHook(() => useQueryParamsManipulation());

  const expectedQueryParams = {searchTerm: 'avatar'};
  expect(result.current.queryParams).toStrictEqual(expectedQueryParams);
});


test('should set query params', () => {

  const {result, rerender} = renderHook(() => useQueryParamsManipulation());

  const newQueryParams = {searchTerm: 'the man'};

  act(() => {
    result.current.setQueryParams(newQueryParams);
  });

  rerender();

  expect(result.current.queryParams).toStrictEqual(newQueryParams);
});


test('should set query params and remove empty keys', () => {

  const {result, rerender} = renderHook(() => useQueryParamsManipulation());

  const newQueryParams = {searchTerm: 'the man'};

  act(() => {
    result.current.setQueryParams({...newQueryParams, year: '', genre: null});
  });

  rerender();

  expect(result.current.queryParams).toStrictEqual(newQueryParams);
});