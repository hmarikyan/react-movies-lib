import {useCallback} from "react";
import {useHistory, useLocation} from "react-router-dom";
import {objectToQueryString, queryStringToObject, removeEmptyProps} from "../Helpers/queryStringHelper";

const useQueryParamsManipulation = () => {
  const history = useHistory();
  const location = useLocation();

  const setQueryParams = useCallback((paramsObject) => {
    let newUrl = location.pathname;
    const newQs = objectToQueryString(removeEmptyProps(paramsObject));
    if(newQs) {
      newUrl += '?'+newQs;
    }
    history.push(newUrl);
  }, [location, history]);

  return {
    setQueryParams,
    queryParams: queryStringToObject(location.search)
  }
}

export default useQueryParamsManipulation;