#React - Movies demo app


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

For production usage

-------------------------------------------------------------

## Dockerize

#### Create docker image

Run  ```docker build -t react-movie-lib .``` 

#### Start container

Run ```docker run -p 80:80 react-movie-lib```.

Open browser at ```http://localhost```


---------------------------------------------------------------


## Demo
Open ```https://react-movies-lib.vercel.app```
